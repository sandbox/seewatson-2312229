1. Installing and using the @font-your-face Context REaction:
=============================================================

- Place the extracted module in sites/all/modules/context_fontyourface
- Go to Administration » Modules and enable @font-your-face Context Integration.
- Go to Administration » Structure » Context and create a context with conditions.
- Add a @font-your-face reaction to the context and check the box to disable font loading when the contions are met.
